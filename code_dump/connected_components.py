import numpy as np
import cv2


def printMatrix(img):
	(verticalLen, horizontalLen) = img.shape
	for v in range(verticalLen):
		for h in range(horizontalLen):
			if int(img[v][h]/255) == 0:
				print('#', end="")
			else:
				print('_', end="")
		print()


def invert(img):
	imagem = cv2.bitwise_not(img)
	return imagem

def cropEssential(sourceImage, inputImage):
    height, width = inputImage.shape
    border = 10
    xlist = []
    ylist = []
    for y in range(width):
        for x in range(height):
            if inputImage[x][y] < 10:
                xlist.append(x)
                ylist.append(y)
    xleft = min(xlist)
    xright = max(xlist)
    ytop = min(ylist)
    ybot = max(ylist)
    cropped = sourceImage[ max(xleft-border, 0): min(xright+border, height), max(ytop-border, 0): min(ybot+border, width)]
    return cropped


#img = cv2.imread('numpy_testing.png', 0)
img = cv2.imread('temporary_files/rlsa.png', 0)
scannedImage = cv2.imread('input/input1.png', 0)

binary_map = (invert(img) > 0).astype(np.uint8)
connectivity = 4 # or 8

output = cv2.connectedComponentsWithStats(binary_map, connectivity, cv2.CV_32S)


scaleFactor = int(255/output[0])

x = img.copy()
x.fill(255)
imageSegments = []
for i in range(output[0]):
	imageSegments.append(x.copy())

(verticalLen, horizontalLen) = img.shape
for v in range(verticalLen):
	for h in range(horizontalLen):
		label = output[1][v][h]
		imageSegments[label][v][h] = 0
		x[v][h] = label*scaleFactor
		
cv2.imwrite('temporary_files/connectedComponents.png', x)

for i in range(1, len(imageSegments)):
	imageSegments[i] = cropEssential(scannedImage, imageSegments[i])
	cv2.imwrite("cropped/cropped_{}.png".format(i), imageSegments[i])
