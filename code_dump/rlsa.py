import numpy as np
import cv2

def printMatrix(img):
	(verticalLen, horizontalLen) = img.shape
	for v in range(verticalLen):
		for h in range(horizontalLen):
			if int(img[v][h]/255) == 0:
				print('#', end="")
			else:
				print('_', end="")
		print()


def RLSA_horizontal(img, window):
	(verticalLen, horizontalLen) = img.shape
	dummy,thresholdedImage = cv2.threshold(img,200,255,cv2.THRESH_BINARY)
	for v in range(verticalLen):
		firstDarkPosition = -1
		count = 0
		for h in range(horizontalLen):
			if thresholdedImage[v][h] == 255:
				count+=1
			else:
				if count<=window and count != 0:
					for i in range(firstDarkPosition+1, h):
						thresholdedImage[v][i] = 0
				count = 0
				firstDarkPosition = h
	
	return thresholdedImage

def RLSA_vertical(img, window):
	(verticalLen, horizontalLen) = img.shape
	dummy,thresholdedImage = cv2.threshold(img,200,255,cv2.THRESH_BINARY)
	for h in range(horizontalLen):
		firstDarkPosition = -1
		count = 0
		for v in range(verticalLen):
			if thresholdedImage[v][h] == 255:
				count+=1
			else:
				if count<=window and count != 0:
					for i in range(firstDarkPosition+1, v):
						thresholdedImage[i][h] = 0
				count = 0
				firstDarkPosition = v
	
	return thresholdedImage

def RLSA(img):
	rlsa_horizontal = RLSA_vertical(RLSA_horizontal(img, 100), 10)
	rlsa_vertical = RLSA_horizontal(RLSA_vertical(img, 100), 10)
	cv2.imwrite('temporary_files/rlsa_horizontal.png', rlsa_horizontal)
	cv2.imwrite('temporary_files/rlsa_vertical.png', rlsa_vertical)
	(verticalLen, horizontalLen) = img.shape
	dummy, blankImage = cv2.threshold(img,0,0,cv2.THRESH_BINARY)

	for v in range(verticalLen):
		for h in range(horizontalLen):
			if(rlsa_horizontal[v][h] == 255 or rlsa_vertical[v][h] == 255 ):
				blankImage[v][h] = 255

	return blankImage

num = 1
img = cv2.imread('input/input{}.png'.format(num), 0)
ret3,th3 = cv2.threshold(img,200,255,cv2.THRESH_BINARY)

rlsa = RLSA(img)
cv2.imwrite('temporary_files/rlsa.png', rlsa)
